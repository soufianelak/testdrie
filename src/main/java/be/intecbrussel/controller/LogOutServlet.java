package be.intecbrussel.controller;

import be.intecbrussel.model.TestDrieService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


@WebServlet("/testdrie")
public class LogOutServlet extends HttpServlet {
    private TestDrieService service = new TestDrieService();


    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        HttpSession ses = request.getSession(false);
        if(ses != null){
            ses.invalidate();
        }
        request.getRequestDispatcher("\"/WEB-INF/login.jsp\"").forward(request, response);
    }

}
