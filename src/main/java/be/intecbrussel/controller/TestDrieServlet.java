package be.intecbrussel.controller;

import be.intecbrussel.Userbean;
import be.intecbrussel.model.TestDrieService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/testdrie")
public class TestDrieServlet extends HttpServlet {
    private static final String BEAN_NAME = "/WEB-INF/login.jsp";
    private TestDrieService service = new TestDrieService();

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        if (request.getSession().getAttribute(BEAN_NAME) != null) {
            request.getRequestDispatcher("/WEB-INF/welcome.jsp").forward(
                    request, response);

        } else {
            request.getRequestDispatcher("/WEB-INF/invalidname.jsp").forward(
                    request, response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        Userbean ub = new Userbean();
        ub.setName(request.getParameter("Name"));
        service.registerU(ub);
        request.getSession().setAttribute(BEAN_NAME, ub);
        request.getRequestDispatcher("/WEB-INF/welcome.jsp").forward(request, response);

    }

}
